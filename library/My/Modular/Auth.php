<?php
class My_Modular_Auth extends Zend_Controller_Plugin_Abstract
{
	
	public function preDispatch( Zend_Controller_Request_Abstract $request )
	{
		$auth = Zend_Auth::getInstance();
		$moduleArray = array('admin');
		
		if(!$auth->hasIdentity() && in_array($request->getModuleName(), $moduleArray))
		{
			$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
			$redirector->setGotoSimple('index', 'index', 'auth');
		}
	}
	
}