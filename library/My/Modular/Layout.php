<?php
class My_Modular_Layout extends Zend_Controller_Plugin_Abstract
{
	
	public function routeShutdown( Zend_Controller_Request_Abstract $request )
	{
		$moduleName = strtolower($request->getModuleName());
		$layout = Zend_Layout::getMvcInstance();
		
		$layout->setLayoutPath( APPLICATION_PATH . '/modules/default/layouts/')
        	->setLayout( 'default' );
								   
		if(file_exists( APPLICATION_PATH . '/modules/'. $moduleName . '/layouts/'.$moduleName.'.phtml'))
		{
			$layout->setLayoutPath( APPLICATION_PATH . '/modules/'.$moduleName.'/layouts/')
        		->setLayout( $moduleName );
		}
	}
	
}