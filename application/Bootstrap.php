<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initRoutes()
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        
        $route = new Zend_Controller_Router_Route(
                'blog/post/:postname',
                array(
                    'controller' => 'index',
                    'action' => 'post',
                    'module' => 'blog'
                )
        );
        
        $router->addRoute('blog', $route);
    }
    
}

