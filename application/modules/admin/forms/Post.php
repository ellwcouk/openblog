<?php

class Admin_Form_Post extends Zend_Form
{

    public function init()
    {
        // title
        $this->addElement('text', 'title', array(
             'filters' => array(
                 'StringTrim'
             ),
            'validators' => array(
                array('StringLength', false, array(1,32))
            ),
            'label' => 'Post Title:',
            'required' => true
        ));
        
        // content
        $this->addElement('textarea', 'content', array(
            'filters' => array(
                'StringTrim'
            ),
            'validators' => array(
                array('StringLength', false, array(1,60000))
            ),
            'required' => true,
            'label' => 'Post Content:'
        ));
        
        // img
        $this->addElement('text', 'img', array(
            'filters' => array(
                
            ),
            'validators' => array(
                array('StringLength', false, array(0,255))
            ),
            'required' => false,
            'label' => 'Path to image:'
        ));
        
        // submit
        $this->addElement('submit', 'submit', array(
            'required' => false,
            'ignore' => true,
            'label' => 'Submit'
        ));
        
    }


}

