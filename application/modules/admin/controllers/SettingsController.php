<?php

class Admin_SettingsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function cacheAction()
    {
        if($this->getRequest()->isPost())
        {
            $cache = Zend_Cache::factory('Core',
                    'File',
                    array(
                        'lifetime' => 7200,
                        'automatic_serialization' => true
                    ),
                    array(
                        'cache_dir' => APPLICATION_PATH . '/data/cache/'
                    ));
            $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
            
            $this->_helper->redirector('index', 'index', 'admin');
        }
    }


}



