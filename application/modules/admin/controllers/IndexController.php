<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        
        
    }

    public function indexAction()
    {
        $storage = Zend_Auth::getInstance()->getStorage()->read();
        $this->view->username = $storage->username;
        $this->view->data = $storage;
    }


}

