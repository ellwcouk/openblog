<?php

class Admin_PostsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $postsModel = new Default_Model_DbTable_Posts();
        $posts = $postsModel->fetchAll(array('createdon' => 'desc'));
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($posts);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->paginator = $paginator;
    }

    public function newAction()
    {
        $form = new Admin_Form_Post();
        
        if( $this->getRequest()->isPost() )
        {
            $formData = $this->getRequest()->getPost();
            if($form->isValid($formData))
            {
                $postsModel = new Default_Model_DbTable_Posts();
                $id = $postsModel->newPost($formData);
                
                $storage = Zend_Auth::getInstance()->getStorage()->read();
                $log = array(
                    'userid' => $storage->userid,
                    'id' => $id,
                    'date' => date('Y-m-d H:i:s')
                );
                $log = serialize($log);
                $logsModel = new Default_Model_DbTable_Logs();
                $logsModel->log($log);
                
                $this->_helper->redirector('index', 'index', 'admin');
            } else {
                $form->populate($formData);
            }
        }
        
        $this->view->form = $form;
    }

    public function editAction()
    {
        if($this->_getParam('id') == '')
        {
            $this->_helper->redirector('index', 'posts', 'admin');
        } else {
            // lets just check it exists....
            $postsModel = new Default_Model_DbTable_Posts();
            $row = $postsModel->fetchRow('postid = "' . $this->_getParam('id') . '"');
            if(!$row)
            {
                $this->_helper->redirector('index', 'posts', 'admin', array('e' => 'Does not exist'));
            }
            
            $form = new Admin_Form_Post();
            if($this->getRequest()->isPost())
            {
                $formData = $this->getRequest()->getPost();
                if($form->isValid($formData))
                {
                    $oldData = $postsModel->fetchRow('postid = "' . $this->_getParam('id') . '"')->toArray();
                    $postsModel->updatePost($this->_getParam('id'), $formData);
                    $storage = Zend_Auth::getInstance()->getStorage()->read();
                    $log = array(
                        'userid' => $storage->userid,
                        'id' => $this->_getParam('id'),
                        'oldcontent' => serialize($oldData),
                        'date' => date('Y-m-d H:i:s')
                    );
                    $log = serialize($log);
                    $logsModel = new Default_Model_DbTable_Logs();
                    $logsModel->log($log);
                    
                    // clean the cache
                    $cache = Zend_Cache::factory('Core',
                        'File',
                        array(
                            'lifetime' => 7200,
                            'automatic_serialization' => true
                        ),
                        array(
                            'cache_dir' => APPLICATION_PATH . '/data/cache/'
                        ));
                    $cache->clean(
                            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
                            array($oldData['title'])
                    );
                    
                    $this->_helper->redirector('index', 'index', 'admin');
                } else {
                    $form->populate($formData);
                }
            } else {
                $row = $row->toArray();
                $form->populate($row);
            }
        }
        
        $this->view->form = $form;
    }


}





