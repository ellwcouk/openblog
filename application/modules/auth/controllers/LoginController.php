<?php

class Auth_LoginController extends Zend_Controller_Action
{

    public function init()
    {
        if(Zend_Auth::getInstance()->hasIdentity())
		{
			$this->_helper->redirector('index', 'index', 'admin');	
		}
    }

    public function indexAction()
    {
        $form = new Auth_Form_Login();
		
		// get the config for the salt
		//$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'auth');
		//echo hash("SHA512", crypt("1234purple", $config->cryptSalt).$config->salt);
		
		//echo My_Utils_Uuid::generate(4);
		if($this->getRequest()->isPost())
		{
			$formData = $this->getRequest()->getPost();
			if($form->isValid($formData))
			{
				$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'auth');
				$password = hash("SHA512", crypt($form->getValue('password'), $config->cryptSalt).$config->salt);
				
				$authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
				$authAdapter->setTableName('users')
					->setIdentityColumn('username')
					->setCredentialColumn('password')
					->setIdentity($form->getValue('username'))
					->setCredential($password);
				$result = Zend_Auth::getInstance()->authenticate($authAdapter);
				if($result->isValid())
				{
					$data = $authAdapter->getResultRowObject(null,'password');
                                        Zend_Auth::getInstance()->getStorage()->write($data);
                                        
                                        //create a log and save to DB
                                        $data = array(
                                            'useragent' => $_SERVER['HTTP_USER_AGENT'],
                                            'ipaddress' => $_SERVER['REMOTE_ADDR'],
                                            'querystring' => $_SERVER['QUERY_STRING']
                                        );
                                        $log = new Default_Model_DbTable_Logs();
                                        $log->log(serialize($data));
                                        
					$this->_helper->redirector('index', 'index', 'admin');
				} else {
					$form->setDescription('Incorrect Details')
						->populate($formData);	
				}
			} else {
				if(count($form->getErrors('token')) > 0)
				{
					$this->_helper->redirector('index', 'login', 'auth', array('e' => 'csrf'));
				}
				$form->setDescription('Incorrect Details')
					->populate($formData);
			}
		}
		
		if( $this->_getParam('e') == 'csrf')
		{
			$form->setDescription('Token Expired. Please try again');	
		}
		
		$this->view->form = $form;
    }


}

