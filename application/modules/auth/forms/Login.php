<?php

class Auth_Form_Login extends Zend_Form
{

    public function init()
    {
        // Username
		$this->addElement('text', 'username', array(
			'filters' => array(
				'StringTrim',
				'StringToLower'
			),
			'validators' => array(
				array('StringLength', false, array(3,20))
			),
			'label' => 'Username:',
			'required' => true
		));
		
		// password
		$this->addElement('password', 'password', array(
			'filters' => array(
			
			),
			'validators' => array(
				array('StringLength', false, array(6,32))
			),
			'label' => 'Password:',
			'required' => true
		));
		
		// Get the keys, create the CAPTCHA service and add it....
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'auth');
		$captchaService = new Zend_Service_ReCaptcha(
			$config->recaptcha->public,
			$config->recaptcha->private,
			null, // we have no params to pass
			array('theme' => 'clean')
		);
		
		$this->addElement('captcha', 'captcha', array(
			'captcha' => 'ReCaptcha',
			'captchaOptions' => array(
				'captcha' => 'ReCaptcha',
				'service' => $captchaService
			),
			'required' => true,
			'label' => 'Please enter the characters you see'
		));
		
		// submit
		$this->addElement('submit', 'login', array(
			'label' => 'Login',
			'required' => false,
			'ignore' => true
		));
		
		// Lets create a token to pass to make sure the POST data comes from our page, not others
		$token = new Zend_Form_Element_Hash('token');
		$token->setSalt(md5(rand(), true)) // this is secure enough for a token
			->setTimeout(300); // 5 mins
		$this->addElement($token);
		
		// We'll want to display an "Failed Authentication" message if nexessary.
		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl', 'class' => 'error')),
			array('Description', array('placement' => 'prepend')),
			'Form'
		));
    }


}

