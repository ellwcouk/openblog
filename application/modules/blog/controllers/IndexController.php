<?php

class Blog_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $postsModel = new Default_Model_DbTable_Posts();
        $posts = $postsModel->fetchAll(array('createdon' => 'desc'));
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($posts);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->paginator = $paginator;
        
    }

    public function postAction()
    {
        $cache = Zend_Cache::factory('Core',
                'File',
                array(
                    'lifetime' => 7200,
                    'automatic_serialization' => true
                ),
                array(
                    'cache_dir' => APPLICATION_PATH . '/data/cache/'
                ));
        
        if($this->_getParam('postname') == '')
        {
            $this->_helper->redirector('index', 'index', 'blog');
        } else {
            $page = $this->_getParam('postname'); 
       }
       $newResult = true;
       if( ($result = $cache->load($page)) === false)
       {
           $postModel = new Default_Model_DbTable_Posts();
           $result = $postModel->getPost($page);
           $result['content'] = '<!-- Cached on: ' . date('Y-m-d H:i:s') . '-->' . $result['content'];
           $cache->save($result, preg_replace("[^A-Za-z0-9]", "", $page));
       }
       
       if($result === false)
       {
           if( ($result = $cache->load('notfound')) === false)
           {
               $postsModel = new Default_Model_DbTable_Posts();
               $result = $postsModel->getPost('Not Found');
               $result['content'] = '<!-- Cached on: ' . date('Y-m-d H:i:s') . '-->' . $result['content'];
               $cache->save($result, 'notfound');
               $result['content'] = $result['content'];
           }
       }
      
       
       $this->view->title = $result['title'];
       $this->view->content = $result['content'];
    }


}



