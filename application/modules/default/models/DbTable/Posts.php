<?php

class Default_Model_DbTable_Posts extends Zend_Db_Table_Abstract
{

    protected $_name = 'posts';

    public function newPost($data)
    {
        $exists = true;
        while($exists == true)
        {
            $id = My_Utils_Uuid::generate(4);
            $select = $this->fetchRow('postid = "' . $id . '"');
            if(!$select)
            {
                $exists = false;
            }
        }
        
        $newData = array(
            'postid' => $id,
            'title' => $data['title'],
            'content' => $data['content'],
            'createdon' => date('Y-m-d H:i:s'),
            'img' => $data['img']
        );
        $this->insert($newData);
        return (string)$id;
    }
    
    public function updatePost($id, $data)
    {
        $newData = array(
            'title' => $data['title'],
            'content' => $data['content'],
            'img' => $data['img']
        );
        
        $this->update($newData, 'postid ="' . $id .'"');
    }
    
    public function getPost($postName)
    {
        $row = $this->fetchRow($this->select()->where('title = ?', $postName));
        if(!$row)
        {
            return false;
        } else {
            return $row->toArray();
        }
    }
    
}

