<?php

class Default_Model_DbTable_Logs extends Zend_Db_Table_Abstract
{

    protected $_name = 'logs';

    public function log($log)
    {
        $exists = true;
        while($exists == true)
        {
            $id = My_Utils_Uuid::generate(4);
            $select = $this->fetchRow('logid = "' . $id . '"');
            if(!$select)
            {
                $exists = false;
            }
        }
        $data = array(
            'logid' => $id,
            'content' => $log,
            'added' => date('Y-m-d H:i:s')
        );
        
        $this->insert($data);
    }
    
}

