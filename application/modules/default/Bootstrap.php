<?php
class Default_Bootstrap extends Zend_Application_Module_Bootstrap
{
	
	protected function _initPlugins()
	{
		$this->bootstrap('Views');
		$front = Zend_Controller_Front::getInstance();
		Zend_Layout::startMvc(); // Used for the layout plugin
		$front->registerPlugin( new My_Modular_Auth())
			->registerPlugin( new My_Modular_Layout());	
	}
	
	protected function _initViews()
	{
		$view = new Zend_View();
		$view->doctype('HTML5');	
	}
	
}