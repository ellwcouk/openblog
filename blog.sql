/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50150
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50150
File Encoding         : 65001

Date: 2012-05-03 15:44:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `logs`
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `logid` char(36) NOT NULL DEFAULT '',
  `content` text,
  `added` datetime DEFAULT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs
-- ----------------------------
INSERT INTO `logs` VALUES ('158f4765-a88d-4e63-82ba-a055efe87b72', 'a:3:{s:6:\"userid\";s:36:\"c241cc68-5311-4b54-aaec-101767106d7a\";s:2:\"id\";s:1:\"0\";s:4:\"date\";s:19:\"2012-05-03 10:08:14\";}', '2012-05-03 10:08:14');
INSERT INTO `logs` VALUES ('2e7a328e-81e6-4176-9d92-ad34aeb4becf', 'a:0:{}', '2012-05-03 11:09:20');
INSERT INTO `logs` VALUES ('5b116a96-2aec-403b-ae5d-eedd7e447664', 'a:0:{}', '2012-05-03 11:09:10');
INSERT INTO `logs` VALUES ('a0e967e4-3a60-4f16-a8dd-aea67a05b51d', 'a:3:{s:6:\"userid\";s:36:\"c241cc68-5311-4b54-aaec-101767106d7a\";s:2:\"id\";O:13:\"My_Utils_Uuid\":8:{s:8:\"\0*\0bytes\";s:16:\"[4{WåEFª-¶ðÒðyÎ\";s:6:\"\0*\0hex\";N;s:9:\"\0*\0string\";s:36:\"815b347b-57e5-4546-aa2d-b6f0d2f079ce\";s:6:\"\0*\0urn\";N;s:10:\"\0*\0version\";N;s:10:\"\0*\0variant\";N;s:7:\"\0*\0node\";N;s:7:\"\0*\0time\";N;}s:4:\"date\";s:19:\"2012-05-03 10:08:41\";}', '2012-05-03 10:08:41');
INSERT INTO `logs` VALUES ('ca2d0100-f872-44cd-8bc2-60f8c35af9bb', 'a:4:{s:6:\"userid\";s:36:\"c241cc68-5311-4b54-aaec-101767106d7a\";s:2:\"id\";s:36:\"27894d5f-48ff-486f-9e5c-f886054e9606\";s:10:\"oldcontent\";s:183:\"a:5:{s:6:\"postid\";s:36:\"27894d5f-48ff-486f-9e5c-f886054e9606\";s:5:\"title\";s:5:\"QAHH!\";s:7:\"content\";s:9:\"xczcxzxzc\";s:9:\"createdon\";s:19:\"2012-05-03 10:09:38\";s:3:\"img\";s:6:\"cxzxzc\";}\";s:4:\"date\";s:19:\"2012-05-03 11:19:10\";}', '2012-05-03 11:19:10');
INSERT INTO `logs` VALUES ('cc6c907a-f6b0-4b18-bc7f-cd297e44abf0', 'a:3:{s:6:\"userid\";s:36:\"c241cc68-5311-4b54-aaec-101767106d7a\";s:2:\"id\";O:13:\"My_Utils_Uuid\":8:{s:8:\"\0*\0bytes\";s:16:\"\'‰M_HÿHož\\ø†N–\";s:6:\"\0*\0hex\";N;s:9:\"\0*\0string\";s:36:\"27894d5f-48ff-486f-9e5c-f886054e9606\";s:6:\"\0*\0urn\";N;s:10:\"\0*\0version\";N;s:10:\"\0*\0variant\";N;s:7:\"\0*\0node\";N;s:7:\"\0*\0time\";N;}s:4:\"date\";s:19:\"2012-05-03 10:09:38\";}', '2012-05-03 10:09:38');
INSERT INTO `logs` VALUES ('cd6c1a4f-08b1-4b2a-945b-e4dd1ba71ea5', 'a:4:{s:6:\"userid\";s:36:\"c241cc68-5311-4b54-aaec-101767106d7a\";s:2:\"id\";s:36:\"27894d5f-48ff-486f-9e5c-f886054e9606\";s:10:\"oldcontent\";s:176:\"a:5:{s:6:\"postid\";s:36:\"27894d5f-48ff-486f-9e5c-f886054e9606\";s:5:\"title\";s:4:\"home\";s:7:\"content\";s:5:\"22222\";s:9:\"createdon\";s:19:\"2012-05-03 10:09:38\";s:3:\"img\";s:4:\"2222\";}\";s:4:\"date\";s:19:\"2012-05-03 15:42:34\";}', '2012-05-03 15:42:34');
INSERT INTO `logs` VALUES ('dbd027ca-654e-496b-9e76-f5aacca6c08c', 'a:3:{s:9:\"useragent\";s:72:\"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:11.0) Gecko/20100101 Firefox/11.0\";s:9:\"ipaddress\";s:9:\"127.0.0.1\";s:11:\"querystring\";s:0:\"\";}', '2012-05-02 18:43:41');
INSERT INTO `logs` VALUES ('df237ecb-97ea-4b42-b9e7-56987e3f0034', 'a:3:{s:6:\"userid\";s:36:\"c241cc68-5311-4b54-aaec-101767106d7a\";s:2:\"id\";s:36:\"5dc0da8e-fb5f-46f9-a5c0-c5ff244ebbe9\";s:4:\"date\";s:19:\"2012-05-03 10:10:21\";}', '2012-05-03 10:10:21');
INSERT INTO `logs` VALUES ('f0588a4d-ca5e-4a98-8661-a1b222e23118', 'a:3:{s:9:\"useragent\";s:72:\"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0\";s:9:\"ipaddress\";s:9:\"127.0.0.1\";s:11:\"querystring\";s:0:\"\";}', '2012-05-03 09:50:28');

-- ----------------------------
-- Table structure for `posts`
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `postid` char(36) NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `createdon` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`postid`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('27894d5f-48ff-486f-9e5c-f886054e9606', 'home2', '22222', '2012-05-03 10:09:38', '2222');
INSERT INTO `posts` VALUES ('5dc0da8e-fb5f-46f9-a5c0-c5ff244ebbe9', 'dsadas', 'dsadasads2222', '2012-05-03 10:10:21', 'dsasda');
INSERT INTO `posts` VALUES ('815b347b-57e5-4546-aa2d-b6f0d2f079ce', 'Not Found', '404 - not found', '2012-05-03 10:08:41', '');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userid` char(36) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` char(128) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `surname` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  PRIMARY KEY (`userid`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('c241cc68-5311-4b54-aaec-101767106d7a', 'james', 'a7726598918ea81d17dac2db9ed78d38ebf79251b3aed2f4f33cfb93746f9d2f88c498fc66f1cd8e741f2293d591b577c091b569704c54c347b2e9b71966f8b2', 'James', 'Elliott', 'elliottwebsites@gmail.com', '2012-05-02 18:35:59');
